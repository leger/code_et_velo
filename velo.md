Définitions
===========


- Cycle: véhicule ayant au moins deux roues et propulsé exclusivement
  par l'énergie musculaire des personnes se trouvant sur ce véhicule, notamment
à l'aide de pédales ou de manivelles. [R311-1]

- Cycle à pédalage assisté : cycle équipé d'un moteur auxiliaire électrique
  d'une puissance nominale continue maximale de 0,25 kilowatt, dont
l'alimentation est réduite progressivement et finalement interrompue lorsque le
véhicule atteint une vitesse de 25 km / h, ou plus tôt si le cycliste arrête de
pédaler. [R311-1]

Remarques:

- Un cycle désigne aussi bien une bicylette, un vélo couché, un tandem, un
  tricycle, ou autre truc étrange.

- Un cycle à pédalage assisté doit avoir une puissance d'assistance inférieur à
  250 W, et doit se désactiver au dela de 25km/h. Sinon il s'agit d'un véhicule
  de catégorie L1e-A ou L1e-B, a.k.a. cyclomoteur. [R311-1]

[R311-1]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000032401189&cidTexte=LEGITEXT000006074228

Éclairage
=========

Préambule
---------

Tous les dispositifs non prévus sont interdits. [R313-1]

Avant
-----

- Obligatoire de nuit et lorsque la visiblilité est faible de jour:
    - Feu blanc ou jaune, aucune indication de distance d'éclairage [R313-4], non
    clignotant [R313-25].
- Obligatoire tout le temps:
    - Catadioptre blanc. [R313-20]

Arrière
-------

- Obligatoire de nuit et lorsque la visiblilité est faible de jour:
    - Feu rouge, nettement visible [R313-5], peut être fixe ou clignotant
    [R313-25].
- Obligatoire tout le temps:
    - Catadioptre rouge, un ou plus. [R313-18]

Visible latéralement
--------------------

- Obligatoire tout le temps:
    - Catadioptre orange [R313-19].

Divers
------

- Obligatoire tout le temps:
    - Catadioptre orange sur les pédales [R313-20].
- Falcultatif:
    - Écarteur de danger [R313-20].

[R313-1]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006841614&cidTexte=LEGITEXT000006074228
[R313-4]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000032401283&cidTexte=LEGITEXT000006074228
[R313-5]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006841623&cidTexte=LEGITEXT000006074228
[R313-18]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000032401343&cidTexte=LEGITEXT000006074228
[R313-19]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000032401346&cidTexte=LEGITEXT000006074228
[R313-20]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000032401351&cidTexte=LEGITEXT000006074228
[R313-25]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000032401362&cidTexte=LEGITEXT000006074228

Avertisseur sonore
==================

- Obligatoire:
    - Timbre ou grelot, audible à 50m. Tout le reste est interdit, mais
      seulement l'utlisation. [R313-33].
- Trompe à son multiple, sireine, sifflets sont interdit d'usage [R416-3]

[R313-33]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006841660&cidTexte=LEGITEXT000006074228
[R416-3]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006842259&cidTexte=LEGITEXT000006074228

Freins
======

Deux dispositifs de freinages (avant et arrière vriassemblablement) efficaces
sont obligatoires [R315-3].

[R315-3]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006841674&cidTexte=LEGITEXT000006074228

Mise en fourrière
=================

Applicable en matière de:

- stationnement en infraction [L325-1]
- l'esthétique des sites et des paysages classés [L325-1]
- la conservation ou l'utilisation normale des voies ouvertes à la circulation
  publique et de leurs dépendances [L325-1], (on peut considérer que le
  stationnement sur les trotoirs gène la circulation de piétons)
- cycles privés d'éléments indispensables à leur utilisation normale et
  insusceptibles de réparation immédiate à la suite de dégradations ou de vols
  [L325-1], aka vélo épaves abandonnés.

[L325-1]: https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006074228&idArticle=LEGIARTI000006841132

Circulation
===========

Règles générales
----------------

- comportement prudent et respecteux, en particulier vis-à-vis des plus
  vulnérables (c'est à dire piétons) [R412-6]
- en position d'effectuer toutes les manœuvres de conduire [R412-6]. Lacher les
  mains, ou une seule pour boire est-il concerné ?
- téléphone interdit, avec ou sans oreillette [R412-6-1]
- écouteurs, que ce soit radio, musique, ou talki interdit [R412-6-1]
- téléphone ou autre truc à écran sur le guidon interdit si cela ne sert pas à
  se guider [R412-6-2]

[R412-6]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000019277061&cidTexte=LEGITEXT000006074228
[R412-6-1]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000030800800&cidTexte=LEGITEXT000006074228
[R412-6-2]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000025111520&cidTexte=LEGITEXT000006074228

Lieu de circulation
-------------------

- Sauf en cas de necessité absolue, sur la chaussé [R412-7].
  - Sauf pour les enfants de 8 ans et moins, si ils circulent au pas [R412-34]
  - Sauf pour les cycles conduits à la main (à pied) [R412-34]
- Circulation sur bande d'arret d'urgence interdite [R412-8]. 
- Circulation à droite autant que de possible, [R412-9]
    - Sauf si la chaussé ne permet pas (déformée, nid-de-poule…) [R412-9]
    - Sauf si c'est pour s'écarter pour se mettre hors de danger des voitures en
      stationnement, [R412-9].
    - Sauf en cas d'une bande cyclable qui n'est pas à droite [R412-9].


Règles de circulation
---------------------

- Les changements de directions doivent se signaler [R412-10].
- Le remontage des files est interdit [R412-24].
- Dans le cas où un vehicule traverse une piste cyclable qui longe sa route, les
  cyclistes ont la priorité quelque soit leurs sens. [R415-3]
- Pour tourner à gauche, les véhicules ont obligation de serrer à gauche (sans
  dépasser l'axe médian), les cyclistes ont également possiblilité de serrer à
  droite [R415-4]

[R415-3]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000019277033&cidTexte=LEGITEXT000006074228
[R415-4]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000019277035&cidTexte=LEGITEXT000006074228

Feux
----

- Dans le cas d'une piste cyclable qui longe un passage piétons, si il n'y a pas
  de feux vélo, alors les cyclistes doivent suivre le feu piéton du passage qui
  est longé par la piste. [R412-30]

- La ligne d'arrêt peut être différente de celle des autres véhicule. C'est ce
  que l'on nomme couramment un sas cyclable. [R415-15]

[R415-15]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000030851501&cidTexte=LEGITEXT000006074228

[R412-7]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000030851458&cidTexte=LEGITEXT000006074228
[R412-8]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000025111522&cidTexte=LEGITEXT000006074228
[R412-9]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000030851463&cidTexte=LEGITEXT000006074228
[R412-10]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006842127&cidTexte=LEGITEXT000006074228
[R412-24]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006842144&cidTexte=LEGITEXT000006074228
[R412-30]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000030851479&cidTexte=LEGITEXT000006074228
[R412-34]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000023095936&cidTexte=LEGITEXT000006074228


À classer
=========

À noter:

Pour les routes limitée à 30 km/h, le régime par défaut est qu'elles soient
toutes à double sens pour les cyclistes sauf décision explicite de l'autorité
locale. [R412-28-1]. Normalement la signalisation est cohérente avec cette
réglementation.

[R412-28-1]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000030839280&cidTexte=LEGITEXT000006074228

Une piste cyclable est considérée comme une voie de la route qu'elle longe sauf
décision contraire de l'autorité pour les priorités. Normalement la
signalisation est cohérente avec cette réglementation.

[R415-14]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006842254&cidTexte=LEGITEXT000006074228

Les véhicules de ramassage des déchets peuvent circuler et s'arreter sur les
bandes cyclables [R412-7], contrairement aux autres conducteurs, ils ne méritent
pas le courroux des cyclistes.

Pour doubler un cycle les autres véhicules (y compris les cycles) ont droit de
chevaucher une ligne continue [R412-19] sans toutefois la franchir.

[R412-19]: https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000030851470&cidTexte=LEGITEXT000006074228

Note: j'en suis à R417-1
